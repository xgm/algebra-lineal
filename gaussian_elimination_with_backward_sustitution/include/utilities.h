#ifndef utilities_h
#define utilities_h

void displayMatrix(double** A, int n);
void displayResults(double* x, int n);

#endif
