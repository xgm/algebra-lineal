#include <iostream>

void displayMatrix(double** A, int n){
	printf("Matriz aumentada [A, b]: \n");
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n + 1; j++){
			if (j == n)
				printf("| ");
			printf("%3.2g ", A[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void displayResults(double* x, int n){
	for (int i = 0; i < n; i++){
		printf("x[%d] = %g\n", i+1, x[i]);
	}
}
