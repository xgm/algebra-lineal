#include <iostream>
#include "../include/gaussMethod.h"
#include "../include/utilities.h"

int main(){
	int n;      /* Tamaño de la matriz */
	double** A; /* Matriz aumentada */
	double* x;  /* Resultados */

	printf("Ingrese el tamaño de la matriz (n x n): ");
	scanf("%d", &n);
	printf("\n");

	A = new double* [n];
	for (int i = 0; i < n; i++)
		A[i] = new double[n+1];

	printf("Ingrese los coeficientes del sistema %d x %d.\n\n", n, n);
	printf("Nota: rellene los coeficientes faltantes con ceros.\n\n");

	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			printf("A[%d][%d] = ", i+1, j+1);
			scanf("%lf", &A[i][j]);
		}
		printf("\n");
	}

	printf("Ingrese los términos independientes.\n\n");
	for (int i = 0; i < n; i++){
		printf("b[%d] = ", i+1);
		scanf("%lf", &A[i][n]);
	}
	printf("\n");

	displayMatrix(A, n);

	x = gaussMethod(A, n);

	displayResults(x, n);

	while(n) delete A[--n];
	delete A;
	delete x;
}
