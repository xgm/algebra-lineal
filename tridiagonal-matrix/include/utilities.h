#ifndef utilities_h
#define utilities_h

double** new_matrix(size_t n, size_t m);
double* new_array(size_t size);
void displayMatrix(double** M, size_t size);
void displayResults(double* x, int n);

#endif
