#include <iostream>
#include "../include/utilities.h"

void gaussMethod(double** A, int n, double*& y){
	int p;
	double m, sum, tmp;

	for (int i = 0; i < n - 1; i++){
		for (p = i; p < n; p++)
			if (p >= i && A[p][i] != 0) break;

		if (p != i) {
			for (int k = 0; k < n+1; k++){
				tmp = A[i][k];
				A[i][k] = A[p][k];
				A[p][k] = tmp;
			}
		}

		for (int j = i + 1; j  < n; j++){
			m = A[j][i] / A[i][i];
			for (int k = 0; k < n + 1; k++)
				A[j][k] = A[j][k] - (m * A[i][k]);
		}
	}

	if (A[n-1][n-1] == 0){
		printf("No existe una única solución\n");
		return;
	}

	y[n] = A[n-1][n] / A[n-1][n-1];

	for (int i = n-2; i >= 0; i--){
		sum = 0;
		for (int j = i+1; j < n; j++)
			sum += A[i][j] * y[j + 1];
		y[i + 1] = (A[i][n] - sum) / A[i][i];
	}
}
