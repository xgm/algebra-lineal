#include <iostream>

double* new_array(size_t size) {
	double* ptr = new double [size]();
	return ptr;
}
double** new_matrix(size_t n, size_t m) {
	double **ptr = new double* [n];
	for (int i = 0; i < n; i++)
		ptr[i] = new double [m]();
	return ptr;
}

void displayMatrix(double** M, size_t size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size + 1; j++) {
			if (j == size)
				printf("| ");
			printf("%g\t", M[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void displayResults(double* x, int n){
	for (int i = 0; i < n; i++)
		printf("y[%d] = %g\n", i, x[i]);
}
