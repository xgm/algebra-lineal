#include <iostream>
#include <cmath>

#include "../include/gaussMethod.h"
#include "../include/utilities.h"

double f(double xi) {
	return -8 * sin(3 * xi) +
			3 * cos(3 * xi) +
			2 * xi + pow(xi, 2) + 2;
}

int main() {
	/* Intervalo */
	double a = -2, b = 2;
	/* Número de particiones */
	int n;
	printf("Ingrese el número de particiones: ");
	scanf("%d", &n);
	/* Longitud de cada subintervalo*/
	double h = (b - a) / n;
	printf("h = %.5lf\n", h);

	/* Puntos de la particion */
	double*  x = new_array(n + 1);
	for (int i = 0; i < n + 1; i++)
		x[i] = a + i * h;

	/* Incognita y(x_{i})*/
	double*  y = new_array(n + 1);
	y[0] = 4.2794;
	y[n] = 3.7206;

	/* Matriz Principal */
	double** M = new_matrix(n - 1, n);

	/* Se llena la matriz tri-diagonal*/
	M[0][0] = 2 * pow(h, 2) - 4;
	M[0][1] = 2 + h;

	M[n - 2][n - 3] = 2 - h;
	M[n - 2][n - 2] = 2 * pow(h, 2) - 4;

	for (int i = 1; i <= n - 3; i++) {
		M[i][i-1] = 2 - h;
		M[i][i]   = 2 * pow(h, 2) - 4;
		M[i][i+1] = 2 + h;
	}

	/* Se adicionan los términos independientes */
	M[0][n-1]   = 2 * pow(h, 2) * f(x[1])   - (2 - h) * y[0];
	M[n-2][n-1] = 2 * pow(h, 2) * f(x[n-1]) - (2 + h) * y[n];

	for (int i = 1; i <= n - 3; i++)
		M[i][n - 1] = 2 * pow(h, 2) * f(x[i + 1]);

	displayMatrix(M, n - 1);

	gaussMethod(M, n-1, y);

	displayResults(y, n + 1);

	/* Exit */
	delete[] x;
	delete[] y;
	for (int i = 0; i < n - 1; i++)
		delete[] M[i];
	delete[] M;

	return 0;
}
