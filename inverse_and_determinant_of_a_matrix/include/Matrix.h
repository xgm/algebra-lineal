#ifndef Matrix_H
#define Matrix_h

class Matrix {
	private:
		int m_size;
		double **M;
		double **Inv;
		double det;

		double** new_matrix(int);
		void del_matrix(double**&, int);

	public:
		Matrix();
		Matrix(const Matrix&);
		Matrix(int);
		~Matrix();

		Matrix& operator = (const Matrix&);
		Matrix operator + (const Matrix&);
		Matrix operator * (const Matrix&);
		Matrix operator * (double);

		bool read_matrix_from_a_file(std::string);
		void display_matrix();
		void display_inverse();
		void inverse();
		double determinant();
};
#endif
