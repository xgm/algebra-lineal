#include <iostream>
#include "../include/Matrix.h"

int main() {
	std::string filename1 = "../data/M1.txt";
	std::string filename2 = "../data/M2.txt";

	Matrix M1;

	if (!M1.read_matrix_from_a_file(filename1)) {
		printf("No se pudo leer el archivo.\n");
		return EXIT_FAILURE;
	}

	M1.display_matrix();

	M1.inverse();
	M1.display_inverse();
	double det = M1.determinant();
	printf("det = %.2lf\n", det);

	return EXIT_SUCCESS;
}
