#include <iostream>
#include <fstream>
#include "../include/Matrix.h"

double** Matrix :: new_matrix(int size) {
	double** ptr = new double* [size];
	for (int i = 0; i < size; i++)
		ptr[i] = new double [size];
	return ptr;
}

void Matrix :: del_matrix(double**& ptr, int size) {
	for (int i = 0; i < size; i++)
		delete[] ptr[i];
	delete[] ptr;
}

// CONSTRUCTORS
Matrix :: Matrix() : M(NULL), Inv(NULL), m_size(0), det(0) {}

Matrix :: Matrix(const Matrix& obj) {
	this->m_size = obj.m_size;
	this->M = new_matrix(this->m_size);

	for (int i = 0; this->m_size; i++)
		for (int j = 0; this->m_size; j++)
			this->M[i][j] = obj.M[i][j];
}

Matrix :: Matrix(int size) {
	m_size = size;
	M = new_matrix(m_size);
}

Matrix :: ~Matrix() {
	if (M != NULL)
		del_matrix(M, m_size);

	if (Inv != NULL)
		del_matrix(Inv, m_size);
}

// METHODS
bool Matrix :: read_matrix_from_a_file(std::string filename) {
	std::ifstream DATA (filename);

	if (!DATA.is_open()){
		printf("No se pudo abrir el archivo.\n");
		return false;
	}

	DATA >> m_size;

	if (m_size <= 0) {
		printf("No es posible crear una matriz de tamaño %d.\n", m_size);
		return false;
	}

	M = new_matrix(m_size);

	for (int i = 0; i < m_size; i++)
		for (int j = 0; j < m_size; j++)
			DATA >> M[i][j];

	DATA.close();

	return true;
}

void Matrix :: display_matrix() {
	if (M == NULL){
		printf("La matrix aun no está definida.\n");
		return;
	}

	printf("Matriz\n");
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++)
			printf("%5.2lf\t", M[i][j]);
		printf("\n");
	}
	printf("\n");
}

void Matrix :: display_inverse() {
	if (Inv == NULL) {
		printf("No se ha calculado la inversa aun.\n");
		return;
	}

	printf("Inversa\n");
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++)
			printf("%5.2lf ", Inv[i][j]);
		printf("\n");
	}
	printf("\n");
}

void Matrix :: inverse() {
	if (M == NULL) {
		printf("No es posible calcular la inversa"
				" sin antes definir la matriz\n");
		return;
	}

	int p;
	double tmp;
	double** m_tmp;

	if (Inv == NULL) {
		Inv = new_matrix(m_size);
		m_tmp = new_matrix(m_size);
	}

	/* Hacer una copia de la matriz permitirá
	 * preservar la original.*/
	for (int i = 0; i < m_size; i++)
		for (int j = 0; j < m_size; j++)
			m_tmp[i][j] = M[i][j];

	/* Crea la matriz identidad */
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++) {
			if (j == i) {
				Inv[i][j] = 1;
				continue;
			}
			Inv[i][j] = 0;
		}
	}

	/* ALGORITMO PARA HALLAR LA INVERSA DE LA MATRIZ
	 * Se construyó en base a una modificación de algoritmo
	 * de eliminación Gaussiana con sustitución hacia atrás
	 */

	for (int i = 0; i < m_size; i++) {
		for (p = i; p < m_size; p++)
			if (p >= i && m_tmp[p][i] != 0) break;

		if (p != i) {
			for (int k = 0; k < m_size; k++){
				tmp = m_tmp[i][k];
				m_tmp[i][k] = m_tmp[p][k];
				m_tmp[p][k] = tmp;

				tmp = Inv[i][k];
				Inv[i][k] = Inv[p][k];
				Inv[p][k] = tmp;
			}
		}

		tmp = m_tmp[i][i];
		for (int j = 0; j < m_size; j++) {
			if (m_tmp[i][j] != 0)
				m_tmp[i][j] /= tmp;

			if (Inv[i][j] != 0)
				Inv[i][j] /= tmp;
		}

		for (int j = 0; j < m_size; j++) {
			if (j == i)
				continue;

			tmp = m_tmp[j][i];
			for (int k = 0; k < m_size; k++) {
				m_tmp[j][k] -= tmp * m_tmp[i][k];
				Inv[j][k]   -= tmp * Inv[i][k];
			}
		}
	}

	del_matrix(m_tmp, m_size);
}

double Matrix :: determinant() {
	double** m_tmp = new_matrix(m_size);
	double m, tmp;
	int p;

	/* Hacer una copia de la matriz permitirá
	 * preservar la original.*/
	for (int i = 0; i < m_size; i++)
		for (int j = 0; j < m_size; j++)
			m_tmp[i][j] = M[i][j];

	for (int i = 0; i < m_size - 1; i++){
		for (p = i; p < m_size; p++)
			if (p >= i && m_tmp[p][i] != 0) break;

		if (p != i) {
			for (int k = 0; k < m_size; k++){
				tmp = m_tmp[i][k];
				m_tmp[i][k] = m_tmp[p][k];
				m_tmp[p][k] = tmp;
			}
		}

		for (int j = i + 1; j < m_size; j++){
			m = m_tmp[j][i] / m_tmp[i][i];
			for (int k = 0; k < m_size; k++)
				m_tmp[j][k] = m_tmp[j][k] - (m * m_tmp[i][k]);
		}
	}

	det = -1;
	for (int i = 0; i < m_size; i++)
		det *= m_tmp[i][i];

	del_matrix(m_tmp, m_size);
	return det;
}

// OPERATORS
Matrix Matrix :: operator + (const Matrix& obj) {
	Matrix newM(m_size);
	for (int i = 0; i < m_size; i++)
		for (int j = 0; j < m_size; j++)
			newM.M[i][j] = M[i][j] + obj.M[i][j];
	return newM;
}

Matrix Matrix :: operator * (double scalar) {
	Matrix newM(m_size);
	for (int i = 0; i < m_size; i++)
		for (int j = 0; j < m_size; j++)
			newM.M[i][j] = M[i][j] * scalar;
	return newM;
}

Matrix Matrix :: operator * (const Matrix& obj) {
	Matrix newObj(m_size);

	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++){
			newObj.M[i][j] = 0;
			// c_{ij} = \sum_{k=1}^{m} a_{ik} + b_{kj}
			for (int k = 0; k < m_size; k++)
				newObj.M[i][j] += M[i][k] * obj.M[k][j];
		}
	}
	return newObj;
}

Matrix& Matrix :: operator = (const Matrix& obj) {
	this->m_size = obj.m_size;
	this->M = new_matrix(this->m_size);

	for (int i = 0; i < this->m_size; i++)
		for (int j = 0; j < this->m_size; j++)
			this->M[i][j] = obj.M[i][j];

	return *this;
}
