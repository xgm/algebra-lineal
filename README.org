* Proyectos de Matemáticas
** Elminación Gaussiana con Sustitución Hacia Atrás
Implementación del algoritmo de eliminación Gaussiana con sustitución
hacia para resolver sistemas de ecuaciones lineales de =n x n=.

/Revisa el proyecto dando click [[./gaussian_elimination_with_backward_sustitution/][aquí]]./

** Clase Matriz y Búsqueda de la Inversa y Determinante.
Se implementó la clase =matriz= que provee las operaciones básicas
entre matrices, además se adicionaron dos algoritmos extras que
permiten hallar la inversa y determinante de un sistema lineal de =n x
n= leido desde un archivo de texto.

/Revisa el proyecto dando click [[./inverse_and_determinant_of_a_matrix/][aquí]]./

** Solución de un Sistema de Ecuaciones Tri-diagonales
Búsqueda de la solución de un sistema de ecuaciones tri-diagonales
utilizando el algoritmo de eliminación Gaussiana con sustitución hacia
atrás.

/Revisa el proyecto dando click [[./tridiagonal-matrix/][aquí]]./

